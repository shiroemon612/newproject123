<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class SystemUser extends Model
{
    protected $table = 'admins';
    protected $connection = 'mongodb';
    public $timestamp = true;
}
