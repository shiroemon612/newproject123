<?php

namespace App;

use Illuminate\Routing\Route;
use Jenssegers\Mongodb\Eloquent\Model as Model;
use DB;

class Department extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'departments';

    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);
    }

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name','status'];

    /*
     * @Author: Bao Long
     * @Description: danh sach department
     * @var array data
     */
    public function getList($data = []){
//        $data['limit'] = isset($data['limit']) ? $data['limit'] : 20;
        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
//      $arr = [];

        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        if(!empty($keyword)){
//            return DB::connection($data['dbname'])->collection('departments')->where($arr)->where('name', 'LIKE', "%$keyword%")->paginate($data['limit']);
            return DB::connection($data['dbname'])->collection('departments')->where($arr)->where('name', 'LIKE', "%$keyword%")->get();
        }

        return DB::connection($data['dbname'])->collection('departments')->where($arr)->get();
//        return DB::connection($data['dbname'])->collection('departments')->where($arr)->paginate($data['limit']);
//        return DB::connection($data['dbname'])->collection('departments')->paginate($data['limit']);
    }
    /*
     * @Author: Bao Long
     * @Description: them 1 department
     * @var array data
     */
    public function create($data = []){
        $data['status'] = isset($data['status']) ? $data['status'] : 1;
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        return DB::connection($data['dbname'])->collection('departments')->insertGetId($arr);
    }
    /*
     * @Author: Bao Long
     * @Description: update 1 department
     * @var array data
     */
    public function updateData($data = []){
        $id = isset($data['_id']) ? $data['_id'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        return DB::connection($data['dbname'])->collection('departments')->where('_id',$id)->update($arr);
    }
    /*
     * @Author: Bao Long
     * @Description: get department parent
     *
     */
    public function getParent($data = []){
        $parent_id = isset($data['parent_id']) ? $data['parent_id'] : '';
        return DB::connection($data['dbname'])->collection('departments')->where('parent_id',$parent_id)->get();
    }
    /*
     * @Author: Bao Long
     * @Description: get department by ID
     *
     */
    public function getById($data = [])
    {
//        return DB::connection($data['dbname'])->collection('departments')->where('_id',$data['_id'])->first();
        return DB::connection($data['dbname'])->collection('departments')->find($data['_id']);
    }

    /*
     * @Author: Bao Long
     * @Description: update active trash
     * @var array data
     */
    public function movetotrash($data = []){
        $id = isset($data['_id']) ? $data['_id'] : '';

        return DB::connection($data['dbname'])->collection('departments')->where('_id',$id)->update(['status' => 0]);
    }
    /*
     * @Author: Bao Long
     * @Description: count staff in department
     * @var array data
     */
    public function countStaff($data = []){
        $departments_id = isset($data['departments_id']) ? $data['departments_id'] : '';
        return DB::connection($data['dbname'])->collection('staffs')->where('departments_id',$departments_id)->count();
    }
    /*
     * @Author: Bao Long
     * @Description: count staff in department
     * @var array data
     */
    public function getManager($data = []){
        $departments_id = isset($data['departments_id']) ? $data['departments_id'] : '';

        return DB::connection($data['dbname'])->collection('staffs')
            ->where('res','trưởng phòng')
            ->where('departments_id',$departments_id)->first();
    }

    /*
     * @Author: Bao Long
     * @Description: lay 1 phòng ban
     * @var array data
     */
    public function getDepartment($data =[]){

        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        return DB::connection($data['dbname'])->collection('departments')->where($arr)->first();


    }
}
