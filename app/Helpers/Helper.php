<?php

namespace App\Helpers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Contracts\Validation;
use Carbon\Carbon;
use DateTime;
use Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class Helper
{
    // use in Blade file: {!! Helper::goHelp() !!}
    // Dùng để test kết nối với các Function Help
    public static function goHelp()
    {
        return 1;
    }

    public static function getError($validata = array())
    {
        $result = [];
        foreach ($validata as $field_name => $messages) {
            if (isset($messages[0])) {
                $result[$field_name] = $messages[0];
            }
        }
        return $result;
    }
    public static function replaceKey($sources = [], $replace = [] ){
        $result = [];
        foreach( $sources as $key => $value ){
            if( isset($replace[$key]) ){
                $result[$key] = $replace[$key];
            }else{
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * @description Lấy ngôn ngữ hiện tại trong hệ thống.
     * @author Then Thach
     * @version 1.0.0
     * @since 1.0.0
     * @return alias của ngôn ngữ hiện tại ( vi, en )
     */
    public static function currentLanguage()
    {
        $language = Session::get('language');
        if (!$language) {
            $language = config('app.locale');
        }
        return $language;
    }
    public static function accessById( $sources, $id ){
        if( is_array($sources) ){
            foreach( $sources as $item ){
                if( isset($item['_id']) && $item['_id'] === $id ){
                    return $item;
                }
            }
        }
        return null;
    }

    public static function translateKey($key, $defaultText = null, $language = null)
    {
        $language = Helper::currentLanguage();
        $findText = Lang::get($key, [], $language);
        if ($findText === $key) {
            return ($defaultText !== null) ? $defaultText : $key;
        }
        return $findText;

    }

    public static function __($key, $defaultText = null, $language = null)
    {
        return self::translateKey($key, $defaultText, $language);
    }

    /**
     * Lấy thông tin user hiện tại
     * @author Then Thach
     * @version 1.0.0
     * @since 1.0.0
     * @return Object - current user
     */
    public static function getCurrentUser()
    {
        if (Session::has('currentUser')) {
            return Session::get('currentUser');
        }
        return null;
    }

    /**
     * @description Lấy thông tin đầy đủ của một user cụ thể
     * @author Then Thach
     * @version 1.0.0
     * @since 1.0.0
     * @param $user - thông tin user cần lấy ( Array)
     * @return string - Họ tên đầy đủ.
     */
    public static function getFullName($user)
    {
        $fullName = '';
        $isFirst = false;
        if (isset($user['first_name'])) {
            $fullName .= ($isFirst) ? $user['first_name'] : sprintf(' %1$s', $user['first_name']);
            $isFirst = true;
        }
        if (isset($user['last_name'])) {
            $fullName .= ($isFirst) ? $user['last_name'] : sprintf(' %1$s', $user['last_name']);
            $isFirst = true;
        }
        if (!empty($fullName)) {
            return $fullName;
        }
        return isset($user['username']) ? $user['username'] : $user['email'];
    }

    /*
        Author: kiemnv@polarisvietnam.com
        Date: 28-02-2017
        Version: 1.0
        Function: seoURL($text='',$number=0, $strtolower=True);
        Return: Trả về chuỗi được xử lý loại bỏ dấu tiếng Việt và các kí tự đặc biệt, sau đó conver Chữ hoa thành chữ thường với $number là số ký tự được cắt ra.
    */
    public static function seoURL($text = '', $number = 0, $strtolower = True)
    {
        $text = html_entity_decode(trim($text), ENT_QUOTES, 'UTF-8');
        $text = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $text);
        $text = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $text);
        $text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $text);
        $text = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $text);
        $text = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $text);
        $text = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $text);
        $text = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $text);
        $text = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $text);
        $text = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $text);
        $text = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $text);
        $text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $text);
        $text = preg_replace("/(đ)/", 'd', $text);
        $text = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $text);
        $text = preg_replace("/(đ)/", 'd', $text);
        $text = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $text);
        $text = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $text);
        $text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $text);
        $text = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $text);
        $text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $text);
        $text = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $text);
        $text = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $text);
        $text = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $text);
        $text = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $text);
        $text = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $text);
        $text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $text);
        $text = preg_replace("/(Đ)/", 'D', $text);
        $text = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $text);
        $text = preg_replace("/(Đ)/", 'D', $text);
        $text = preg_replace('/[^A-z0-9]/', '-', $text);
        $text = preg_replace('/-{2,}/', '-', $text);
        $text = str_replace("^^", '', $text);
        $text = str_replace("[", "", $text);
        $text = str_replace("]", "", $text);
        $text = trim($text, '-');
        if ($strtolower = true) {
            $text = strtolower($text);
        }
        if ($number > 0) {
            $text = substr($text, 0, $number);
            $text = trim($text, '-');
        }
        return $text;
    }


    /*
        Author: kiemnv@polarisvietnam.com
        Date: 28-02-2017
        Version: 1.0
        Function: convertTime($time='',$lang='');
        $time: là định dạng thời gian theo hàm time()
        Return: Trả về thời gian theo ngôn ngữ.
    */
    public static function convertTime($date, $format)
    {
        $time = strtotime($date);
        $time = date($format, $time);
        return $time;
    }

    public static function convertTime2($ngay, $format)
    {
        $time = $ngay['date'];
        $time = Carbon::parse($time)->toCookieString();
        return $time;
    }

    public static function convertTime3($ngay, $format)
    {
        $time = $ngay['date'];
        $time = Carbon::parse($time)->toDateString();
        return $time;
    }


    /*
        @author: kiemnv@polarisvietnam.com
        @since: 28-02-2017
        @version: 1.0
        @var link: là được dẫn Full URL
        @return: Trả về Domain.
    */
    public static function convertHost($link)
    {
        return parse_url($link, PHP_URL_HOST);
    }


    public static function UploadIMG($image, $store = null)
    {
        if (!$store) {
            $store = 'images/questions';
        }
        if ($image != null) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $a = $image->move(public_path($store), $imageName);
            if ($a) {
                return $imageName;
            }
            return '';
        }
        return '';
    }

    public static function UploadIMGV1($image, $name, $store = null)
    {
        if (!$store) {
            $store = 'images/questions';
        }
        if ($image != null) {
            $name .= '.' . $image->getClientOriginalExtension();
            $upload = $image->move(public_path($store), $name);
            if ($upload) {
                return $name;
            }
        }
        return '';
    }

    //upload file cho admin user
    public static function UploadIMGV2($image)
    {
        if ($image != null) {
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $a = $image->move(public_path('images/avatars'), $imageName);
            if ($a) {
                return $imageName;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    //upload file cho disc
    public static function UploadIMGV3($image, $name, $store = null)
    {
        if (!$store) {
            $store = 'images/disc';
        }
        if ($image != null) {
            $name .= '.' . $image->getClientOriginalExtension();
            $upload = $image->move(public_path($store), $name);
            if ($upload) {
                return $name;
            }
        }
        return '';
    }
    /**
     * @description Đọc data từ cache
     * @author Then Thach
     * @version 1.0.0
     * @since 1.0.0
     * @param $key
     * @param null $language
     * @return array
     */
    public static function getCache($key, $language = null)
    {
        if (!$language) {
            $language = config('app.locale');
        }
        $getKey = sprintf('%1$s_%2%s', $key, $language);
        if (Cache::has($getKey)) {
            return Cache::get($getKey);
        }
        $CacheData = [
            'allowlanguage' => Config('app.locales'),
            'language' => $language,
            'title' => '',
            'image' => '',
            'description' => '',
            'keywords' => '',
            'robots' => 'index,follow',
            'sliders' => [
                0 => ['name' => 'name', 'image' => 'slide-01.jpg', 'thumb' => 'banner-caption.jpg', 'caption' => 'ABA Company in Japan', 'date' => 'Application 15 Feb – 15 Mar 2017', 'link' => 'http://zxcvbnm.com'], //slide-02.jpg
                1 => ['name' => 'name', 'image' => 'slide-02.jpg', 'thumb' => 'banner-caption-1.jpg', 'caption' => 'SunTari', 'date' => 'Apply to us Family!', 'link' => ''],
                2 => ['name' => 'name', 'image' => 'slide-03.jpg', 'thumb' => 'banner-caption-2.jpg', 'caption' => 'Thach Bich', 'date' => 'Thach Bich Company', 'link' => ''],
            ],
            // 'cities' => [
            // 	0 => ['alias'=>'da-nang','name'=>'Đà Nẵng'],
            // 	1 => ['alias'=>'hue','name'=>'Huế'],
            // ],
            // 'jobs' => [
            // 	0 => ['alias'=>'da-nang','name'=>'Đà Nẵng'],
            // 	1 => ['alias'=>'hue','name'=>'Huế'],
            // ],

            'themes' => [
                'phone' => ['content' => '1111110909 889988', 'language' => 'vi'],
                'email' => ['content' => 'jobtest@gmail.com'],
                'address' => ['content' => '363 – 365, Pham Ngu Lao, Pham Ngu Lao Ward, District 1, HCMC'],
                'facebook' => ['content' => 'http://facebook.com'],
                'twitter' => ['content' => 'http://twitter.com'],
                'linkedin' => ['content' => 'http://linkedin.com'],
            ],
            'topcommnents' => [
                0 => ['userID' => '', 'name' => 'name', 'avatar' => 'client.jpg', 'content' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout', 'sort' => '1'],
                1 => ['userID' => '', 'name' => 'name', 'avatar' => 'client.jpg', 'content' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout', 'sort' => '1'],
                2 => ['userID' => '', 'name' => 'name', 'avatar' => 'client.jpg', 'content' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout', 'sort' => '1'],
            ],
            'topEmployes' => [
                0 => ['logo' => 'partner1.png', 'link' => ''],
                1 => ['logo' => 'partner2.png', 'link' => ''],
                2 => ['logo' => 'partner3.png', 'link' => ''],
                3 => ['logo' => 'partner4.png', 'link' => ''],
                4 => ['logo' => 'partner5.png', 'link' => ''],
                5 => ['logo' => 'partner.png', 'link' => ''],
            ],
            'topCandidate' => [
                0 => ['image' => 'cadidate.png', 'link' => 'dfgdff', 'name' => 'jennifer lawrence'],
                1 => ['image' => 'cadidate-1.png', 'link' => 'dfgdff', 'name' => 'jennifer lawrence'],
                2 => ['image' => 'cadidate-2.png', 'link' => 'dfgdff', 'name' => 'jennifer lawrence'],
            ],
            'listTypeTests' => [
                0 => ['image' => 'icon-01.png', 'link' => 'dfgdff', 'title' => 'Logical'],
                1 => ['image' => 'icon-02.png', 'link' => 'dfgdff', 'title' => 'Numberical'],
                2 => ['image' => 'icon-03.png', 'link' => 'dfgdff', 'title' => 'Verbal'],
                3 => ['image' => 'icon-04.png', 'link' => 'dfgdff', 'title' => 'Abstract'],
                4 => ['image' => 'icon-05.png', 'link' => 'dfgdff', 'title' => 'Diagrammatic'],
                5 => ['image' => 'icon-06.png', 'link' => 'dfgdff', 'title' => 'Data/Error Checking'],
                6 => ['image' => 'icon-07.png', 'link' => 'dfgdff', 'title' => 'Faults Diagnosis'],
                7 => ['image' => 'icon-08.png', 'link' => 'dfgdff', 'title' => 'Mechanical'],
            ],
            'menus' => [
                0 => [
                    'title' => 'Psychometric Tests',
                    'alias' => '/',
                    'sub1' => [
                        0 => [
                            'title' => 'Aptitude Tests',
                            'alias' => '/',
                            'sub2' => [
                                0 => ['title' => 'Logical Reasoning Test', 'alias' => '/',],
                                1 => ['title' => 'Numberical Reasoning', 'alias' => '/',],
                                2 => ['title' => 'Verbal Reasoning', 'alias' => '/',],
                                3 => ['title' => 'Abstract Reasoning Test', 'alias' => '/',],
                                4 => ['title' => 'Diagrammatic Reasoning Test ', 'alias' => '/',],
                                5 => ['title' => 'Data / Error Checking Test', 'alias' => '/',],
                                6 => ['title' => 'Faults Diagnosis Test', 'alias' => '/',],
                                7 => ['title' => 'Mechanical Reasoning Test', 'alias' => '/',],
                                8 => ['title' => 'Technical Reasoning Test', 'alias' => '/',],
                            ],
                        ],
                        1 => [
                            'title' => 'DISC Tests',
                            'alias' => '/',
                            'sub2' => [
                            ],
                        ],
                        2 => [
                            'title' => 'Personality Test',
                            'alias' => '/',
                            'sub2' => [
                                0 => ['title' => 'Big 5 Traits Personality Test', 'alias' => '/',],
                                1 => ['title' => 'Full Big 5 Traits Personality Test', 'alias' => '/',],
                            ],
                        ],
                        // 7 => [
                        //     'title' => 'Assessment Centers',
                        //     'alias' => '/',
                        //     'sub2' => [
                        //         0 => ['title' => 'Case Study', 'alias' => '/',],
                        //         1 => ['title' => 'Written Exercises', 'alias' => '/',],
                        //         2 => ['title' => 'In-Tray and E-Tray Exercises', 'alias' => '/',],
                        //         3 => ['title' => 'Role Plays', 'alias' => '/',],
                        //         4 => ['title' => 'Presentations', 'alias' => '/',],
                        //         5 => ['title' => 'Group Exercises', 'alias' => '/',],
                        //     ],
                        // ],
                    ],

                ],
                1 => [
                    'title' => 'Competency Models',
                    'alias' => '/',
                    'sub1' => [],
                    // 'sub1' => [
                    //     0 => [
                    //         'title' => 'Industry',
                    //         'alias' => '/',
                    //         'sub2' => [
                    //             0 => ['title' => 'Airlines and Tourism', 'alias' => '/',],
                    //             1 => ['title' => 'Automotive', 'alias' => '/',],
                    //             2 => ['title' => 'Agriculture', 'alias' => '/',],
                    //             3 => ['title' => 'Chemicals', 'alias' => '/',],
                    //             4 => ['title' => 'Communication and Media', 'alias' => '/',],
                    //             5 => ['title' => 'Construction and Real Estate', 'alias' => '/',],
                    //             6 => ['title' => 'Education & Training', 'alias' => '/',],
                    //             7 => ['title' => 'Banking & Financial Services and Insurance', 'alias' => '/',],
                    //             8 => ['title' => 'FMCG', 'alias' => '/',],
                    //             9 => ['title' => 'Logistics and Forwarding', 'alias' => '/',],
                    //             10 => ['title' => 'Healthcare & Medical', 'alias' => '/',],
                    //             11 => ['title' => 'IT', 'alias' => '/',],
                    //             12 => ['title' => 'Life Sciences', 'alias' => '/',],
                    //             13 => ['title' => 'Manufacturing', 'alias' => '/',],
                    //             14 => ['title' => 'Oil & Gas And Energy', 'alias' => '/',],
                    //             15 => ['title' => 'Public Sector And NGO', 'alias' => '/',],
                    //             16 => ['title' => 'Retail', 'alias' => '/',],
                    //             17 => ['title' => 'Technology', 'alias' => '/',],
                    //             18 => ['title' => 'Others', 'alias' => '/',]
                    //         ],
                    //     ],
                    //     1 => [
                    //         'title' => 'Function',
                    //         'alias' => '/',
                    //         'sub2' => [
                    //             0 => ['title' => 'HSE', 'alias' => '/',],
                    //             1 => ['title' => 'Security', 'alias' => '/',],
                    //             2 => ['title' => 'Accounting)', 'alias' => '/',],
                    //             3 => ['title' => 'Internal Audit', 'alias' => '/',],
                    //             4 => ['title' => 'Finance& Investment', 'alias' => '/',],
                    //             5 => ['title' => 'Human Resources', 'alias' => '/',],
                    //             6 => ['title' => 'Export & Import', 'alias' => '/',],
                    //             7 => ['title' => 'Sales', 'alias' => '/',],
                    //             8 => ['title' => 'Procurement / Purchasing', 'alias' => '/',],
                    //             9 => ['title' => 'Admin& Clerical', 'alias' => '/',],
                    //             10 => ['title' => 'Executive Management', 'alias' => '/',],
                    //             11 => ['title' => 'Customer Services', 'alias' => '/',],
                    //             12 => ['title' => 'Advertising / PR & Marketing', 'alias' => '/',],
                    //             13 => ['title' => 'Information Technology', 'alias' => '/',],
                    //             14 => ['title' => 'QA &QC', 'alias' => '/',],
                    //             15 => ['title' => 'Operations', 'alias' => '/',],
                    //             16 => ['title' => 'Architecture & Design', 'alias' => '/',],
                    //             17 => ['title' => 'Legal', 'alias' => '/',],
                    //             18 => ['title' => 'Strategy', 'alias' => '/',],
                    //             19 => ['title' => 'Maintenance & Repair', 'alias' => '/',],
                    //             20 => ['title' => 'Project Management', 'alias' => '/',],
                    //             21 => ['title' => 'Interpretation & Translation', 'alias' => '/',],
                    //             22 => ['title' => 'Consulting', 'alias' => '/',],
                    //             23 => ['title' => 'Construction', 'alias' => '/',],
                    //             24 => ['title' => 'Production', 'alias' => '/',],
                    //             25 => ['title' => 'Others', 'alias' => '/',],
                    //         ],
                    //     ],
                    //     2 => [
                    //         'title' => 'Job Family & Job Title',
                    //         'alias' => '/',
                    //         'sub2' => [
                    //         ],
                    //     ],
                    //     3 => [
                    //         'title' => 'Job Levels',
                    //         'alias' => '/',
                    //         'sub2' => [
                    //             0 => ['title' => 'New Graduates / Entry Level', 'alias' => '/',],
                    //             1 => ['title' => 'Experienced Non-manager', 'alias' => '/',],
                    //             2 => ['title' => 'Supervisor / Manager', 'alias' => '/',],
                    //             3 => ['title' => 'Director and above', 'alias' => '/',],
                    //         ],
                    //     ],
                    // ],
                ],
                2 => [
                    'title' => 'Interview',
                    'alias' => '/',
                    'sub1' => [
                        // 0 => ['title' => 'Interview Guide and Preparation', 'alias' => '/', 'sub2' => [],],
                        // 1 => ['title' => 'Competency Based Interview and STAR', 'alias' => '/', 'sub2' => [],],
                        // 2 => ['title' => 'Informal Interviews', 'alias' => '/', 'sub2' => [],],
                        // 3 => ['title' => 'How to answer BEI-STAR Interview Questions', 'alias' => '/', 'sub2' => [],],
                        // 4 => ['title' => 'Types of BEI-STAR Interview Questions', 'alias' => '/', 'sub2' => [],],
                        // 5 => ['title' => 'CV Writing', 'alias' => '/', 'sub2' => [],],
                        // 6 => ['title' => 'CV Samples', 'alias' => '/', 'sub2' => [],],
                        // 7 => ['title' => 'Job Interview Tips ', 'alias' => '/', 'sub2' => [],]
                    ],
                ],
                3 => [
                    'title' => 'Our Service',
                    'alias' => '/',
                    'sub1' => [
                        // 0 => ['title' => 'Career Advice and Orientation', 'alias' => '/', 'sub2' => [],],
                        // 1 => ['title' => 'Competency Assessment', 'alias' => '/', 'sub2' => [],],
                        // 2 => ['title' => 'Competency Verification', 'alias' => '/', 'sub2' => [],],
                        // 3 => ['title' => 'Build Competency Frameworks', 'alias' => '/', 'sub2' => [],],
                        // 4 => ['title' => 'Recruitment Services', 'alias' => '/', 'sub2' => [],],
                        // 5 => ['title' => 'Organization Design & Development', 'alias' => '/', 'sub2' => [],],
                        // 6 => ['title' => 'Training', 'alias' => '/', 'sub2' => [],],
                        // 7 => ['title' => 'Succession Plan', 'alias' => '/', 'sub2' => [],],
                        // 8 => ['title' => 'Recruitment Services', 'alias' => '/', 'sub2' => [],],
                        // 9 => ['title' => 'Talent Assessment and Development', 'alias' => '/', 'sub2' => [],],
                        // 10 => ['title' => 'Leadership Assessment and Development', 'alias' => '/', 'sub2' => [],],
                        // 11 => ['title' => 'Consult BSC & KPIs', 'alias' => '/', 'sub2' => [],],
                        // 12 => ['title' => 'Employee Surveys', 'alias' => '/', 'sub2' => [],]
                    ],
                ],
                4 => [
                    'title' => 'Candidate',
                    'alias' => '/candidate',
                    'sub1' => [
                    ],
                ],
                5 => [
                    'title' => 'Employer',
                    'alias' => '/employer',
                    'sub1' => [
                    ],
                ],
            ],
            // 'sliders' => Slider::where('language',$language)->get()->toArray(),
            // 'themes' => Theme::where('language',$language)->get()->toArray(),
            'cities' => App\City::where('language', $language)->get()->toArray(),
            'countries' => App\Countries::where('language', $language)->get()->toArray(),
            'jobs' => App\Jobs::where('language', $language)->get()->toArray(),
            'postJobs' => App\JobPost::where('language', $language)->take(20)->get()->toArray(),

            //
        ];
        Cache::put($getKey, $CacheData, 1000);
        return $CacheData;
    }


    /**
     * @description Tạo 1 mongo Model mới dựa key & value của 1 mảng cho trước
     * @author Thạch Thên
     * @version 1.0.0
     * @since 1.0.0
     * @var $model : Model hiện tại cần set các thuộc tính từ mảng.
     * @var $data : mảng giá trị nguồn dùng để set giá trị cho model
     * @return array
     *  + data: mảng sau khi loại bỏ các key không hợp lệ
     *  + object: Model sau khi đã set các giá trị từ mảng
     */
    public static function getDocument($model = Model, $data = array())
    {
        // trả về null nếu đối biến $model không phải là 1 đối tượng Model
        if (!$model instanceof Model) {
            return null;
        }
        $hold_array = [];
        // lấy tất cả giá trị trong mảng trong trường hợp giá trị fillable không được định nghĩa
        if (count($model->getFillable()) === 0) {
            $t = self::getFromArray($data);
            return [
                'data' => $t->data,
                'object' => $t->object
            ];
        }
        // duyệt danh sách mảng giá trị đầu vào theo $key & $value
        foreach ($data as $key => $value) {
            if (in_array($key, $model->getFillable())) {
                $hold_array[$key] = $value;
                $model->{$key} = $value;
            }
        }
        // trả về mảng bao gồm giá trị hợp lê & đối tượng Model sau khi set thuộc tính hợp lệ tương ứng
        return [
            'data' => $hold_array,
            'object' => $model
        ];

    }

    /**
     * Lấy danh sách các key hợp lệ của 1 Model
     * @author Thạch Thên
     * @version 1.0.0
     * @since 1.0.0
     * @var
     * $resul : Model cần set thuộc tính từ mảng truyền vào
     * $ata : mảng các giá trị cần set theo $key & $value
     * @return array
     *  + data: mảng sau khi loại bỏ các key không hợp lệ
     *  + object: Model sau khi đã set các giá trị từ mảng
     */
    private static function getFromArray($result = Model, $data = array())
    {
        $hold_array = [];
        // duyệt danh sách mảng giá trị đầu vào theo $key & $value
        foreach ($data as $key => $value) {
            $hold_array[$key] = $value;
            $result->{$key} = $value;
        }
        return [
            'data' => $hold_array,
            'object' => $result
        ];
    }

    // Hàm tạo Key
    public static function generateKey($id, $option, $select)
    {
        return $option . '.' . $id . '.' . md5('jobtest' . $id . $select);
    }

    // Hàm đối chiếu lại Key cho Aptitude
    public static function unGenerateKeyAptitude($string)
    {
        $arr = explode(".", $string);
        if ($arr[2] == md5('jobtest' . $arr[1] . "true")) {
            return [$arr[0], true];
        } else {
            return [$arr[0], false];
        }
    }

    // Hàm đối chiếu lại Key cho DISC
    public static function unGenerateKeyDISC($string)
    {
        $arr = explode(".", $string);
        return [$arr[0], true];
    }

    public static function getScoreDISC($arr)
    {
        arsort($arr);
        $new = [];
        foreach ($arr as $key => $value) {
            $new[] = [
                'number' => $value,
                'value' => $key
            ];
        }
        $select = [];
        if ($new[1]['number'] == $new[2]['number']) {

            if ($new[0]['value'] == 'd' || $new[0]['value'] == 'i') {
                $select[0] = 'd';
                $select[1] = 'i';
            }
            if ($new[0]['value'] == 's' || $new[0]['value'] == 'c') {
                $select[0] = 's';
                $select[1] = 'c';
            }
        } else {
            $select[0] = $new[0]['value'];
            $select[1] = $new[1]['value'];
        }

        return $select;
    }

    // Hàm convert tính ra các thang comment
    public static function getComment($type, $recomment, $percent)
    {
        switch ($type) {
            case "aptitude":
                if ($percent <= 16) {
                    $exComment = ['level' => 'very_low', 'content' => $recomment['description']['very_low'], 'title' => 'Very Low'];
                } elseif ($percent > 16 & $percent <= 33) {
                    $exComment = ['level' => 'low', 'content' => $recomment['description']['low'], 'title' => 'Low'];
                } elseif ($percent > 33 & $percent <= 68) {
                    $exComment = ['level' => 'average', 'content' => $recomment['description']['average'], 'title' => 'Average'];
                } elseif ($percent > 68 & $percent <= 84) {
                    $exComment = ['level' => 'high', 'content' => $recomment['description']['high'], 'title' => 'High'];
                } elseif ($percent > 84 & $percent <= 100) {
                    $exComment = ['level' => 'very_high', 'content' => $recomment['description']['very_high'], 'title' => 'Very High'];
                }
                return $exComment;
                break;
            default;
                return false;
        }
    }

    public static function convertLevelPersonnality($percent)
    {
        if ($percent <= 16) {
            return 'very_low';
        } elseif ($percent > 16 & $percent <= 33) {
            return 'low';
        } elseif ($percent > 33 & $percent <= 68) {
            return 'average';
        } elseif ($percent > 68 & $percent <= 84) {
            return 'high';
        } elseif ($percent > 84 & $percent <= 100) {
            return 'very_high';
        }
        return false;
    }

    /**
     * @description Lấy giá trị số của một phần tử trong mảng
     * @author Then Thach
     * @version 1.0.0
     * @since 1.0.0
     * @param $source - Array mảng cần lấy
     * @param $key - string key cần kiểm tra
     * @return int
     */
    public static function getNumber( $source, $key ){
        return ( isset( $source[$key] ) && is_numeric($source[$key]) ) ? $source[$key] : 0;
    }

    /**
     * Kiểm tra app_key truyền từ api qua có hợp lệ hay không
     * @author HUU PHAC
     * @version 1.0.0
     * @since 1.0.0
     *
     * @param app_key
     * @return result['fail'] nếu không hợp lệ, ngược lại result['success']
     */
    public static function canAccess($request)
    {
        $result = null;

        $rs = $request->route()->getName();
        $list = explode('.', $rs)[0];
        $result['success'] = true;
        if ($list == 'api') {
            if ($request->app_key !== null) {
                // kiểm tra app_key
                $app_key = config('app.key');
                if (!$app_key) {
                    $result['fail']['app_key'] = 'app_key not set';
                    unset($result['success']);
                } else {
                    // so khớp app_key
                    if ($app_key !== $request->app_key) {
                        $result['fail']['app_key'] = 'app_key is not valid';
                        unset($result['success']);
                    }
                }
            } else {
                // app_key không tồn tại
                $result['fail']['app_key'] = 'app_key not found';
                unset($result['success']);
            }
        }

        return $result;
    }

    /**
     * lấy level job_competencies
     * @author Nhật Huy
     * @param $source
     * @return true
     */
    public static function isLevel($source = [], $key)
    {
        foreach ($source as $s) {
            if (isset($s['value']) && strval($s['value']) === strval($key)) {
                return true;
            }
        }
        return false;
    }

    public static function isLevelv1($source = [], $key, $name)
    {
        foreach ($source as $s) {
            if (isset($s[$name]) && (strval($s[$name]) == strval($key))) {
                return true;
            }
        }
        return false;
    }


    /**
     * @description Lấy danh sách các route có quyền truy cập của user hiện tại
     * @author HuuPhac
     * @Version 1.0.0
     * @Since 1.0.0
     * @return mixed
     */
    public static function getActionOfUser()
    {
        $currentUser = self::getCurrentUser();
        $user_role = $currentUser['role'];
        $rs = App\Role::where('_id', $user_role)->select('cans')->first();
		return ($rs) ? $rs->cans : [];
    }

    public static function currentUserCan($key, $user = null)
    {
        if (!$user) {
            $user = self::getCurrentUser();
        }
        if (!$user) {
            return false;
        }
        $user_role = $user['role'];
        $cans = App\Role::where('_id', $user_role)->select('cans')->first();
        if (!$cans) {
            return false;
        }
        return in_array($key, $cans);

    }

    /**
     * lấy alias child personality
     * @author Nhật Huy
     * @return $alias|null
     */
    public static function getPersonalityChild($source)
    {
        if (isset($source)) {
            $child_name = App\Recomment::find($source);
            return $child_name['alias'];
        } else {
            return null;
        }
    }

    /**
     * lấy name role admin user
     * @author Nhật Huy
     */
    public static function getRoleUser($source)
    {
        if (isset($source)) {
            $role = App\Role::where([
                '_id' => $source
            ])->first();
            return $role['name'];
        } else {
            return null;
        }
    }

    /**
     * kiểm tra aptitude recommned
     * @author Nhật Huy
     */
    public static function checkRecommendAptitude($source)
    {
        if (isset($source)) {
            $recommend = App\Recomment::where([
                'aptitude_id' => $source,
            ])->first();
            if (count($recommend) > 0) {
                return $recommend;
            } else {
                return false;
            }
        }
    }
    /**
     * kiểm tra personality recommned
     * @author Nhật Huy
     */
    public static function checkRecommendPersonality($source)
    {
        if (isset($source)) {
            $recommend = App\Recomment::where([
                'alias' => $source,
            ])->first();
            if (count($recommend) > 0) {
                return $recommend;
            } else {
                return false;
            }
        }
    }

}
