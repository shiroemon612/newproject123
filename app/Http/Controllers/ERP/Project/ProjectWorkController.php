<?php

namespace App\Http\Controllers\ERP\Project;

use App\ERPModels\GroupTask;
use App\ERPModels\Staff;
use App\ERPModels\Tasks;
use App\ERPModels\Project;
use Session;
use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProjectWorkController extends Controller
{
	//
	public function getRenderWorkProject(Request $request)
	{
		$dbname = Session::get('dbname');
		if (Session::has('idProject')) {
			$idProject = Session::get('idProject');
			if (empty($idProject)) {
				return redirect()->back()->withErrors('ID Project Not Found!');
			} else {
				$modelGroupTask = new GroupTask();
				$modelStaff = new Staff();
				$modelTasks = new Tasks();
				$dataGroupTask = $modelGroupTask::all(); // id group
				$dataStaff = $modelStaff::all();
				// show all
				$dataTask = $modelTasks::all();

				foreach ($dataGroupTask as $groupTask) {
					if(isset($groupTask['_id'])) {
						$arrGroupId[] = $groupTask['_id'];
					}
				}
				$dataTaskGetByGroupId = [];
				foreach($arrGroupId as $groupId) {
					$taskGetByGroupId = $modelTasks::where('group_task_id', '=', $groupId)
						->where('parent_id', '=', '')
						->where('status', '=', 0)
						->get();
					$dataTaskGetByGroupId[] = $taskGetByGroupId;
				}

				$idLeader = null;
				$holdData = [];
				foreach ($dataTask as $task) {
					foreach ($task['task_staff'] as $user) {
						if (isset($user['action'])) {
							if ($user['action'] == 1) {
								$idLeader = $user['id'];
								break;
							}
						}
					}
					$item = $user;
					if ($idLeader) {
						$item['leader'] = DB::connection($dbname)
							->collection('staffs')
							->where('_id', $idLeader)
							->pluck('name')
							->first();
					} else {
						$item['leader'] = null;
					}

					$holdData[] = $item;
				}
				$leader = $holdData;
				unset($holdData);

				return view('erp.project.work', [
					'idProject'            => $idProject,
					'dataGroupTask'        => $dataGroupTask,
					'dataStaff'            => $dataStaff,
					'dataTaskGetByGroupId' => $dataTaskGetByGroupId,
					'leaderTask'           => $leader,

				]);
			}
		}
	}

}
