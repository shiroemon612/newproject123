<?php

namespace App\Http\Controllers\ERP\Project;

use App\ERPModels\GroupTask;
use App\ERPModels\Staffs;
use App\ERPModels\Project;
use App\Http\Controllers\Controller;
use App\ERPModels\Tasks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Session;
use Config;
use Response;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
	public function getEditProject(Request $request)
	{
		$dbname = Session::get('dbname');
		$id = $request->id;
		if (empty($id)) {
			return Response::json([
				'data'   => '',
				'status' => 'false',
				'msg'    => 'failed',
			]);
		} else {
			$data = DB::connection($dbname)
				->collection('projects')
				->where('_id', $id)
				->get();

			return Response::json([
				'data'   => $data,
				'status' => 'true',
				'msg'    => 'Success',
			]);
		}
	}

	public function postAddGroupTaskProject(Request $request)
	{
		$idProject = $request->idProject;
		if (empty($idProject)) {
			return Response::json([
				'msg'    => 'ID Project Not Found!',
				'status' => 'false',
			]);
		} else {
			$nameGroupTask = $request->nameGroupTask;
			$agrs = [
				'project_id' => $idProject,
				'name'       => $nameGroupTask,
			];
			$modelGroupTask = new GroupTask();
			$excute = $modelGroupTask->__createGroupTask($agrs);
			$idGroupCreated = $modelGroupTask->_id;
			if ($excute) {
				return Response::json([
					'msg'            => 'Add Group Task Success!',
					'status'         => 'true',
					'idGroupCreated' => $idGroupCreated,
				]);
			} else {
				return Response::json([
					'msg'            => 'Add Group Task Failed!',
					'status'         => 'false',
					'idGroupCreated' => '',
				]);
			}
		}
	}

	public function postEditGroupTaskProject(Request $request)
	{
		$idGroupTask = $request->idGroupTask;
		if (empty($idGroupTask)) {
			return Response::json([
				'msg'    => 'ID Project Not Found!',
				'status' => 'false',
			]);
		} else {
			$nameGroupTask = $request->nameGroupTask;
			$agrs = [
				'_id'  => $idGroupTask,
				'name' => $nameGroupTask,
			];
			$modelGroupTask = new GroupTask();
			$excute = $modelGroupTask->__updateGroupTask($agrs);
			if ($excute) {
				return Response::json([
					'msg'    => 'Update Name Group Task Success!',
					'status' => 'true',
				]);
			} else {
				return Response::json([
					'msg'    => 'Update Group Task Failed!',
					'status' => 'false',
				]);
			}
		}
	}

	public function postDeleteGroupTaskProject(Request $request)
	{
		$idGroupTask = $request->idGroupTask;
		if (empty($idGroupTask)) {
			return Response::json([
				'msg'    => 'ID Group Task Not Found!',
				'status' => 'false',
			]);
		} else {
			$agrs = [
				'_id' => $idGroupTask,
			];
			$modelGroupTask = new GroupTask();
			$excute = $modelGroupTask->__deleteGroupTask($agrs);
			if ($excute) {
				return Response::json([
					'msg'    => 'Delete Group Task Success!',
					'status' => 'true',
				]);
			} else {
				return Response::json([
					'msg'    => 'Delete Group Task Failed!',
					'status' => 'false',
				]);
			}
		}
	}

	public function postAddTaskToGroupProject(Request $request)
	{
		$idProject = $request->idProject;
		$idGroupTask = $request->idGroupTask;
		if (empty($idProject) && empty($idGroupTask)) {
			return Response::json([
				'msg'    => 'ID Group Task And ID Project Not Found!',
				'status' => 'false',
			]);
		} else {
			$arrDataListJob = $request->dataListJob;
			$nameJob = $arrDataListJob['nameJob'];
			$startDateJob = $arrDataListJob['startDateJob'];
			$endDateJob = $arrDataListJob['endDateJob'];
			$description = $arrDataListJob['description'];
			$memberTask = $arrDataListJob['memberTask'];
			$staff = [];
			foreach ($memberTask as $member) {
				$staff[] = [
					'id'     => $member,
					'action' => 1,
				];
			}
			$arrStaff = $staff;
			$managerTask[] = [
				'id'     => $arrDataListJob['managerTask'],
				'action' => 0,
			];
			$taskStaff = array_merge($managerTask, $arrStaff);
			$agrs = [
				'idProject'    => $idProject,
				'idGroupTask'  => $idGroupTask,
				'nameJob'      => $nameJob,
				'startDateJob' => $startDateJob,
				'endDateJob'   => $endDateJob,
				'desc'         => $description,
				'taskStaff'    => $taskStaff,
			];
			$modelTasks = new Tasks();
			$excute = $modelTasks->__createTask($agrs);
			$idTaskCreated = $modelTasks->id;
			$dataStaffOfTask = $modelTasks::find($idTaskCreated)['task_staff'];
			$data = [
				'idTaskCreated'   => $idTaskCreated,
				'dataStaffOfTask' => $dataStaffOfTask,
			];

			if ($excute) {
				return Response::json([
					'data'   => $data,
					'msg'    => 'Add Task Success!',
					'status' => 'true',
				]);
			} else {
				return Response::json([
					'data'   => null,
					'msg'    => 'Add Task Failed!',
					'status' => 'false',
				]);
			}
		}

	}

	public function postDeleteTaskProject(Request $request)
	{
		$idTask = $request->idTask;
		if (empty($idTask)) {
			return Response::json([
				'msg'    => 'ID Task Not Found!',
				'status' => 'false',
			]);
		} else {
			$agrs = [
				'_id' => $idTask,
			];
			$modelTasks = new Tasks();
			$excute = $modelTasks->__deleteTempTask($agrs);
			if ($excute) {
				return Response::json([
					'msg'    => 'Delete Task Success!',
					'status' => 'true',
				]);
			} else {
				return Response::json([
					'msg'    => 'Delete Task Failed!',
					'status' => 'false',
				]);
			}
		}
	}

	public function postRequestEditTask(Request $request)
	{
		$idTask = $request->idTask;
		if (empty($idTask)) {
			return Response::json([
				'msg'    => 'ID Task Not Found!',
				'status' => 'false',
			]);
		} else {
			$modelTasks = new Tasks();
			$dataTask = $modelTasks::find($idTask);
			if ($dataTask) {
				return Response::json([
					'data'   => $dataTask,
					'msg'    => 'Success!',
					'status' => 'true',
				]);
			} else {
				return Response::json([
					'data'   => null,
					'msg'    => 'Failed!',
					'status' => 'false',
				]);
			}
		}
	}

	public function postEditTaskProject(Request $request)
	{
		$taskId = $request->taskId;
		if (empty($taskId)) {
			return Response::json([
				'msg'    => 'ID Task Not Found!',
				'status' => 'false',
			]);
		} else {
			$dataListJob = $request->dataListJob;
			$nameJob = $dataListJob['nameEditJob'];
			$startDateJob = $dataListJob['editStartDateJob'];
			$endDateJob = $dataListJob['editEndDateJob'];
			$description = $dataListJob['description'];
			$memberTask = $dataListJob['memberTask'];
			$staff = [];
			foreach ($memberTask as $member) {
				$staff[] = [
					'id'     => $member,
					'action' => 1,
				];
			}
			$arrStaff = $staff;
			$managerTask[] = [
				'id'     => $dataListJob['managerTask'],
				'action' => 0,
			];
			$taskStaff = array_merge($managerTask, $arrStaff);
			$agrs = [
				'_id'          => $taskId,
				'nameJob'      => $nameJob,
				'startDateJob' => $startDateJob,
				'endDateJob'   => $endDateJob,
				'desc'         => $description,
				'taskStaff'    => $taskStaff,
			];
			$modelTask = new Tasks();
			$excute = $modelTask->__updateTask($agrs);
			if ($excute) {
				return Response::json([
					'msg'    => 'Update Task Success!',
					'status' => 'true',
				]);
			} else {
				return Response::json([
					'msg'    => 'Update Task Failed!',
					'status' => 'false',
				]);
			}
		}
	}


}