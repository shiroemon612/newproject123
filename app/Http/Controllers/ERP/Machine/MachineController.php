<?php

namespace App\Http\Controllers\ERP\Machine;


use Session;
use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\ERPModels\Machines;

class MachineController extends Controller
{
    //
    public function index(Request $request)
    {
        $dbname = Session::get('dbname');
        $modelMachine = new Machines;
        $data = [];
        $args = [
            'dbname' => $dbname,
            'status' => 1,
        ];
        $machines = $modelMachine->__getList($args)->toArray();
//        $machines = $machines;
        $data['machines'] = $machines;
        return view('erp.machine.machine',$data);
    }
    public function store(Request $request)
    {
        $requestData = $request->all();
        $result = [
            'success' => true,
            'data' => ''
        ];
        $dbname = Session::get('dbname');
        $modelMachine = new Machines;
        $requestData = $request->all();

        $requestData['dbname'] = $dbname;

        $rules = [
            'name' => 'required',

        ];
        $validator = \Validator::make($requestData, $rules);
        if ($validator->fails()) {
            return response()->json(array('result' => false,'error' => $validator->errors()),200);

        }
        $result = $modelMachine->__create($requestData);
        $requestData['_id'] = $result;


//        return $result;
        return response()->json(array('result' => true,'data' => $requestData),200);
    }
}