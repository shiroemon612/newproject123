<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class LogoutController extends Controller
{
    public function logout() {
        Session::forget('user');
        return redirect('/');
    }
}
