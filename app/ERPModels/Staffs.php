<?php

namespace App\ERPModels;

use Illuminate\Database\Eloquent\Model;

class Staffs extends Model
{
    //
	protected $collection = 'staffs';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'name',
		'image',
		'email',
		'birthday',
		'gender',
		'personal_id',
	    'phone',
	    'address',
	    'bank_acc',
	    'bank_name',
	    'assuren',
	    'contracts',
	    'work_history',
	    'departmens_id',
	    'res',
	    'users_id',
	    'relation',
		'status',
	];
    /*
     * @Author: Bao Long
     * @Description: danh sach employer
     * @var array data
     */
    public function __getList($data = []){
        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        if(!empty($keyword)){
            return DB::connection($data['dbname'])->collection('staffs')->where($arr)->where('name', 'LIKE', "%$keyword%")->get();
        }
        return DB::connection($data['dbname'])->collection('staffs')->where($arr)->get();
    }

    /*
     * @Author: Bao Long
     * @Description: them 1 employer
     * @var array data
     */
    public function __create($data = []){
        $data['status'] = isset($data['status']) ? $data['status'] : 1;
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        return DB::connection($data['dbname'])->collection('staffs')->insertGetId($arr);
    }

    /*
     * @Author: Bao Long
     * @Description: update 1 staff
     * @var array data
     */
    public function __updateData($data = []){
        $id = isset($data['_id']) ? $data['_id'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        if (isset($data['company_name_his'])) {
            $his_count = count($data['company_name_his']);
            for($i = 0 ; $i < $his_count ; $i++){
                $companyHis = isset($data['company_name_his'][$i]) ? $data['company_name_his'][$i] : '';
                $timeWork = isset($data['time_work'][$i]) ? $data['time_work'][$i] : '';
                $desWork = isset($data['des_his'][$i]) ? $data['des_his'][$i] : '';
                if(!empty($companyHis)){
                    $his = new \stdClass();
                    $his->company_name = $companyHis;
                    $his->time = $timeWork;
                    $his->des = $desWork;
                    $arr['work_history'][] = array($his);
                }
            }
        }
        return DB::connection($data['dbname'])->collection('staffs')->where('_id',$id)->update($arr);

    }

    /*
     * @Author: Bao Long
     * @Description: get staff by ID
     *
     */
    public function __getById($data = [])
    {
        return DB::connection($data['dbname'])->collection('staffs')->find($data['_id']);
    }
}
