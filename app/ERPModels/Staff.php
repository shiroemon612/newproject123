<?php

namespace App\ERPModels;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use DB;
use App\Models\SystemUser;
use Session;

class Staff extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'staffs';
    protected $connection = 'mongodb';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code','name','ngaysinh','gender','image','personal_id','phone','address','bank_acc','bank_name',
        'assuren','contracts','work_history','departments_id','res','users_id','relation','email','status',
        '_id','is_out'
    ];

	public function __construct()
	{
		$dbname = Session::get('dbname');
		$this->connection = $dbname;
	}

    /*
     * @Author: Bao Long
     * @Description: danh sach employer
     * @var array data
     */
    public function getList($data = []){
        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
      $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        if(!empty($keyword)){
            return DB::connection($data['dbname'])->collection('staffs')->where($arr)->where('name', 'LIKE', "%$keyword%")->get();
        }
        return DB::connection($data['dbname'])->collection('staffs')->where($arr)->get();
    }
    /*
     * @Author: Bao Long
     * @Description: them 1 employer
     * @var array data
     */
    public function create($data = []){
        $data['status'] = isset($data['status']) ? $data['status'] : 1;
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        $user = new SystemUser;
        $user->username = $data['email'];
        $user->password = $data['email'];
        $user->email = $data['email'];
        $user->dbname = Session::get('dbname');
        $user->role = 1;
        $user->status = 0;
        $user->save();

        return DB::connection($data['dbname'])->collection('staffs')->insertGetId($arr);
    }
    /*
     * @Author: Bao Long
     * @Description: update 1 staff
     * @var array data
     */
    public function updateData($data = []){
        $id = isset($data['_id']) ? $data['_id'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        if (isset($data['company_name_his'])) {
            $his_count = count($data['company_name_his']);
            for($i = 0 ; $i < $his_count ; $i++){
                $companyHis = isset($data['company_name_his'][$i]) ? $data['company_name_his'][$i] : '';
                $timeWork = isset($data['time_work'][$i]) ? $data['time_work'][$i] : '';
                $desWork = isset($data['des_his'][$i]) ? $data['des_his'][$i] : '';
                if(!empty($companyHis)){
                    $his = new \stdClass();
                    $his->company_name = $companyHis;
                    $his->time = $timeWork;
                    $his->des = $desWork;
                    $arr['work_history'][] = array($his);
                }
            }
        }
        $email = DB::connection($data['dbname'])->collection('staffs')->where('_id', $id)->pluck('email')->first();
        DB::connection('mongodb')->collection('admins')->where('username', $email)->update(['email' => $data['email'], 'username' => $data['email']]);
        return DB::connection($data['dbname'])->collection('staffs')->where('_id',$id)->update($arr);

    }

    /*
     * @Author: Bao Long
     * @Description: get staff by ID
     *
     */
    public function getById($data = [])
    {
//        return DB::connection($data['dbname'])->collection('staffs')->where('_id',$data['_id'])->first();
        return DB::connection($data['dbname'])->collection('staffs')->find($data['_id']);
    }

    /*
     * @Author: Bao Long
     * @Description: update active trash
     * @var array data
     */
    public function movetotrash($data = []){
        $id = isset($data['_id']) ? $data['_id'] : '';

        return DB::connection($data['dbname'])->collection('staffs')->where('_id',$id)->update(['status' => 0]);
    }

    /*
     * @Author: Bao Long
     * @Description: ajaxGetEmail
     * @var array data
     */
    public function ajaxGetEmail($data =[]){
        $email = isset($data['email']) ? $data['email'] : '';
        $getEmail = DB::connection($data['dbname'])->collection('staffs')->where('email',$email)->first();
        if($getEmail == false) {
            return DB::connection('mongodb')->collection('admins')->where('email',$email)->first();
        }
        return $getEmail;
    }
    /*
     * @Author: Bao Long
     * @Description: AjaxGetPersonaId
     * @var array data
     */
    public function ajaxGetPersonaId($data =[]){
        $personal_id = isset($data['personal_id']) ? $data['personal_id'] : '';
        $getPersonalId = DB::connection($data['dbname'])->collection('staffs')->where('personal_id',$personal_id)->first();
        return $getPersonalId;
    }
    /*
     * @Author: Bao Long
     * @Description: check email or person_id
     * @var array data
     */
    public function checkEmalOrPersonal($data =[])
    {
        $personal_id = isset($data['personal_id']) ? $data['personal_id'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        if (DB::connection($data['dbname'])->collection('staffs')->where('personal_id', $personal_id)->first() == true) {
            return true;
        }else if (DB::connection($data['dbname'])->collection('staffs')->where('email',$email)->first() == true){
           return true;
        } else if (DB::connection('mongodb')->collection('admins')->where('username',$email)->first() == true){
           return true;
        }
        return false;
    }
    /*
     * @Author: Bao Long
     * @Description: xu ly update khi upload file
     * @var array data
     */
    public function processDataUpdateFromFile($data =[]){
        $personal_id = isset($data['personal_id']) ? $data['personal_id'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        DB::connection('mongodb')->collection('admins')->where('username', '111zxzxzx.solution@gmail.com')->update([
           // 'username' => $email,
            'email' => $email
        ]);

        if($personal_id != ""){
            return DB::connection($data['dbname'])->collection('staffs')->where('personal_id',$personal_id)->update($arr);
        }
        elseif($email !=""){
            return DB::connection($data['dbname'])->collection('staffs')->where('email',$email)->update($arr);
        }

    }
    /*
     * @Author: Bao Long
     * @Description: lay 1 nhan vien
     * @var array data
     */
    public function getUser($data =[]){

        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        return DB::connection($data['dbname'])->collection('staffs')->where($arr)->first();
    }

    public function getNameById($data = [])
    {
        return DB::connection($data['dbname'])->collection('staffs')->where('_id', $data['_id'])->pluck('name')->first();
    }

    public function ajaxCheck($data =[]){

        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        return DB::connection($data['dbname'])->collection('staffs')->where($arr)->first();
    }
}