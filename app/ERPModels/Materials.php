<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materials extends Model
{
    //
	protected $collection = 'materials';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'name',
		'unit',
		'quantity',
		'unit_price',
		'buy_date',
		'status',
	];
}
