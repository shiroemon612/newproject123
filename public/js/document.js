﻿
// $('a.createDocument').click(function () {
//     var doc = $(this).closest("#doc3").attr("id");
//     if (doc == "doc3") {
//         $(".form-control.place").closest(".col-md-6.col-xs-12").css({ "display": "none" })
//     }
//     else { $(".form-control.place").closest(".col-md-6.col-xs-12").css({ "display": "block" }) }
// });

//validate  edit document {menu project}
$(function () {
    $("#form-editDocument").validate({
        rules: {
            editTitleDoc: "required",
            // editFile: "required",
        },
        messages: {
            editTitleDoc: "Vui lòng nhập tên công văn ",
            // editFile: "Vui lòng chọn file",
        }, submitHandler: function (form) {
            //code
        }
    });
});
//validate  create document {menu project}
$(function () {
    $("#form-createDocument").validate({
        rules: {
            titleDoc: "required",
            // createFile: "required",
        },
        messages: {
            titleDoc: "Vui lòng nhập tên công văn ",
            // createFile: "Vui lòng chọn file ",
        }, submitHandler: function (form,event) {
            event.preventDefault();
            // var t = $('.tbl-web-search').DataTable();
            // t.fnAddData([
            //     1,
            //     2,
            //     3,
            //     4,
            //     5,
            //     6,
            //     7,
            //     8,
            //     9,
            //     10,
            //     11
            // ]);
            // return;
            var form = $('form#form-createDocument')
            var formData = new FormData(form[0]);
            // var obj = {};

            // var element = form.find('.form-data');
            // $(element).each(function () {
            //     obj[$(this).attr('name')] = $(this).val();
            //
            // })
            var url = form.attr('action');
            var t = $('#doc1 .tbl-web-search').DataTable();
            t.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                var data = this.data();
                console.log(data);

                data[0] = parseInt(data[0])+1;

                this.data(data);
            });
            // var size_table = t.data().length;


            // return;
            // var row = $('#doc1 tr.hide').clone();
            // row.removeClass('hide');
            // row.appendTo('#doc1 tbody');
            // return;

            $.ajax({
                type: 'POST',
                url: url,
                // data: obj,
                // dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (getdata) {
                    if(getdata.result){
                        console.log(getdata.data.name);
                        var id = ( ( getdata.data._id.$oid === null  ) ? '' : getdata.data._id.$oid);
                        var titleDoc = getdata.data.name !== undefined ? getdata.data.name : '';
                        var numDoc = getdata.data.code !== undefined ? getdata.data.code : '';
                        var dateDoc = getdata.data.date !== undefined ? getdata.data.date : '';
                        var to = getdata.data.to !== undefined ? getdata.data.to : '';
                        var desc = getdata.data.desc !== undefined ? getdata.data.desc : '';
                        var project = getdata.data.project !== undefined ? getdata.data.project : '';
                        var department_name = getdata.data.department_name !== undefined ? getdata.data.department_name : '';
                        // var file = getdata.data.file !== undefined ? getdata.data.file : '';
                        var file_url = getdata.data.file_url !== undefined ? ('<a href="'+getdata.data.file_url+'">File</a>') : '';
                        var note = getdata.data.note !== undefined ? getdata.data.note : '';
                        var row = $('#doc1 tr.hide').clone();
                        row.find('[data-name="titleDoc"]').text(titleDoc);
                        row.find('[data-name="numDoc"]').text(numDoc);
                        row.find('[data-name="dateDoc"]').text(dateDoc);
                        row.find('[data-name="to"]').text(to);
                        row.find('[data-name="desc"]').text(desc);
                        // row.find('[data-name="project"]').text(project);
                        // row.find('[data-name="department"]').text(department);
                        // row.find('[data-name="file"]').text(file);
                        row.find('[data-name="note"]').text(note);
                        row.removeClass('hide');
                        //row.appendTo('#doc1 tbody');
                        var t = $('.tbl-web-search').DataTable();
                        var i = t.row.add([
                            // ++size_table,
                            1,
                            titleDoc,
                            numDoc,
                            dateDoc,
                            to,
                            desc,
                            project,
                            department_name,
                            file_url,
                            note,
                            row.find('[data-name="btn"]').html()
                        ]).draw(false);
                        t.rows(i).nodes().to$().attr("data-id", id);


                        // console.log(cloneHTML.removeClass('.hide'));
                    }



                    // $('.table').find('tbody').append(cloneHTML);
                    //
                    // //cloneHTML.find('.name').text(getdata.data.name);
                    // console.log(getdata);
                    // if (getdata.result) {
                    //
                    //     form.trigger("reset");
                    //     $('#createMachines').modal('hide');
                    // }
                }
            });
            return false;
        }
    });
});

$('.nav-tabs li').click(function () {
    var child = $(this).find('a');
    var aria_controls = child.attr('aria-controls');
    var form = $('#form-createDocument');
    var group_name = form.find('#group_name');
    if(aria_controls == undefined){
        return
    }
    switch (aria_controls){
        case 'doc1':
            group_name.val(0);
            $(".form-control.place").closest(".col-md-6.col-xs-12").css({ "display": "block" });
            $(".form-control.place-from").closest(".col-md-6.col-xs-12").css({ "display": "none" });
            break;
        case 'doc2':
            group_name.val(1);
            $(".form-control.place").closest(".col-md-6.col-xs-12").css({ "display": "none" });
            $(".form-control.place-from").closest(".col-md-6.col-xs-12").css({ "display": "block" });
            break;
        case 'doc3':
            group_name.val(2);
            $(".form-control.place").closest(".col-md-6.col-xs-12").css({ "display": "none" });
            $(".form-control.place-from").closest(".col-md-6.col-xs-12").css({ "display": "none" });
            break;
    }
    // alert(child.attr('aria-controls'))
});
