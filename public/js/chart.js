﻿DevExpress.viz.currentTheme("generic.light");
$(function () {
    var dataSource = [{
        day: "Monday",
        oranges: 3
    }, {
        day: "Tuesday",
        oranges: 2
    }, {
        day: "Wednesday",
        oranges: 3
    }, {
        day: "Thursday",
        oranges: 4
    }, {
        day: "Friday",
        oranges: 6
    }, {
        day: "Saturday",
        oranges: 11
    }, {
        day: "Sunday",
        oranges: 4
    }];

    $("#chart-weekly").dxChart({
        dataSource: dataSource,
        series: {
            argumentField: "day",
            valueField: "oranges",
            name: "Chart Weekly",
            type: "bar",
            color: '#ffaa66'
        }
    });
});

