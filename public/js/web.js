// "use strict";
(function (window) {
    window.Web = new Web();

    //window.Web.project.test('dhsgdus');
})(window);

function WebBK() {
    var self = this;
    this.init = function () {
        self.project();
        self.projectWork();
    },
        self.project = function () {
            // <select class="onSelect" url="/ssfs/sd/sdf" removeID="IDJob">
            $('.onClick').unbind('click').bind('click', function (event) {
                event.preventDefault(event);
                var url = $(this).attr('url');
                var id = $(this).attr('data-id');
                var _token = $("meta[name='csrf-token']").attr("content");
                if (url === undefined) {
                    return;
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        "_token": _token,
                        "id": id,
                    },
                    cache: false,
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                    },
                    success: function (data) {
                        console.log(data);
                        var name = data.data[0]['name'];
                        var startDate = data.data[0]['start'];
                        var endDate = data.data[0]['end'];
                        var description = data.data[0]['description'];
                        var managerId = data.data[0]['project_staff'][0].id; // id manager cu~
                        $('option[id=' + managerId + ']').attr("selected", "selected");
                        var managerName = $('option[id=' + managerId + ']').html();
                        $('.chosen-single > span').html(managerName);
                        $('#form-editProject .idProject').html('<input type="hidden" value="' + id + '" name="idProject"/>');
                        $('#form-editProject .name-project').html('<input type="text" class="form-control" value="' + name + '" name="nameEdit" id="nameEdit" />');
                        $('#form-editProject .startDate').html('<input data-provide="datepicker" class="form-control" value="' + startDate + '" name="startEdit" id="startEdit" placeholder="mm/dd/yyyy" />');
                        $('#form-editProject .endDate').html('<input data-provide="datepicker" class="form-control" value="' + endDate + '" name="endEdit" id="endEdit" placeholder="mm/dd/yyyy" />');
                        $('#form-editProject .descriptionProject').html('<textarea class="form-control" name="descriptionEdit" rows="3" placeholder="Nhập mô tả..."> ' + description + ' </textarea>');
                    },
                    error: function (xhr, textStatus, errorThrown) {
                    },
                    timeout: 3000,
                });
            });
        },

        self.projectWork = function () {
            $('.group-task').unbind('click').bind('click', function () {
                var idGroupTask = $(this).attr('group-task-id');
                var nameGroupTask = $(this).attr('group-task-name');
                var group = $('#eidtGroup');
                group.find('#txt_editGroup').val(nameGroupTask);
                if (!group.find('form input[name="idGroupTask"]').length) {
                    group.find('form').append('<input type="hidden" name="idGroupTask" value="' + idGroupTask + '" />');
                } else {
                    group.find('form input[name="idGroupTask"]').remove();
                    group.find('form').append('<input type="hidden" name="idGroupTask" value="' + idGroupTask + '" />');
                }
            });

            $('.create-task-to-group').unbind('click').bind('click', function () {
                var idGroupTask = $(this).attr('group-task-id');
                var idProject = $(this).attr('project-id');
                var group = $('#createJob');
                if (!group.find('form input[name="idGroupTask"]').length && !group.find('form input[name="idProject"]').length) {
                    group.find('form').append('<input type="hidden" name="idGroupTask" value="' + idGroupTask + '" />');
                    group.find('form').append('<input type="hidden" name="idProject" value="' + idProject + '" />');
                } else {
                    group.find('form input[name="idGroupTask"]').remove();
                    group.find('form input[name="idProject"]').remove();
                    group.find('form').append('<input type="hidden" name="idGroupTask" value="' + idGroupTask + '" />');
                    group.find('form').append('<input type="hidden" name="idProject" value="' + idProject + '" />');
                }
            });

            $('#managerTask').change(function () {
                var removeMember = $('#managerTask option:selected').val();
                console.log($(this).attr("member-remove"));
                var remove_name = $(this).attr("member-remove-name");
                var remove_id = $(this).attr("member-remove-id");
                if (remove_id == undefined) {
                    $(this).attr("member-remove-id", removeMember);
                    $(this).attr("member-remove-name", $('#managerTask option:selected').text());
                    $('#memberTask option').each(function () {
                        if ($(this).val() == removeMember) {
                            $('#memberTask option[value="' + $(this).val() + '"]').remove();
                        }
                    });
                }
                else {
                    $('#memberTask').append('<option value="' + remove_id + '">' + remove_name + '</option>');
                    $(this).attr("member-remove-id", removeMember);
                    $(this).attr("member-remove-name", $('#managerTask option:selected').text());
                    $('#memberTask option').each(function () {
                        if ($(this).val() == removeMember) {
                            $('#memberTask option[value="' + $(this).val() + '"]').remove();
                        }
                    });
                }
                $('#memberTask').trigger("chosen:updated");
                $('#memberTask').trigger("change");
            });
        }
}


function ProjectWork(){
    var projectWork = this;

    return projectWork;
}

function Project(){
    var project = this;
    project.test = function(m){
        alert(m);
    }

    project.ajax = function ( el ) {
        var element = $(el);
        if( element.length < 1 ){ return; }

        element.unbind('click').bind('click', function (e) {
            e.preventDefault(e);
            if (confirm('Are you sure?')) {
                var idTask = $(this).attr('task-id');
                var url = $(this).attr('url-ajax');
                var _token = $("meta[name='csrf-token']").attr("content");
                if (url === undefined) {
                    return;
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        "_token": _token,
                        "idTask": idTask,
                    },
                    cache: false,
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                    },
                    success: function (data) {
                        console.log(data);
                        deleteTask(idTask);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                    },
                    timeout: 3000,
                });
            } else {
                return;
            }
        });
    }


    return project;
}

function Web(){
    var self = this;
    var construct = function(){
        self.project = new Project();
        self.projectWork = new ProjectWork();
    }
    // khoi tao
    construct();
    return self;
}
