@extends('erp.layouts.app')

@section('content')
    <div class="group-breadcrumb">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <li id="menueml">Dự án</li>
            <li class="active">Nhân sự</li>
        </ol>

    </div>
<div class="content-page">
    <div class="group-box" style="border-bottom: 1px solid #fafafa; box-shadow: 0 1px 0 #d8d8d8;">
        <div class="group-btn pull-left">

            <h3 class="title-page">Quản lý nhân sự</h3>
            <h5 class="des-page">Quản lý công việc của từng nhân viên trong phòng ban. </h5>
        </div>
        <div class="pull-right">

        </div>
    </div>
    <div class="group-employee">
        <div class="works-group">
            <table id="table-mobile" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Tên</th>
                        <th>CV cần làm</th>
                        <th>CV đang triển khai</th>
                        <th>CV hoàn thành</th>
                        <th>Ghi chú</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn B</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn C</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                    <tr>
                        <td>Nguyên Văn A</td>
                        <td>03</td>
                        <td>12</td>
                        <td>2</td>
                        <td><a href="#" class="link-detail-work">Chi tiết</a></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="works-detail">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class=" box-widget widget-user">
                <div class="widget-user-header bg-aqua-active">
                    <div class="widget-user-image" style="background-image: url(images/img2.jpg)"></div>
                    <div class="widget-content">
                        <h3 class="widget-user-username">Nguyên Văn A</h3>
                        <h5 class="widget-user-desc">Admin</h5>
                    </div>

                </div>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#need" aria-controls="basic" role="tab" data-toggle="tab">Cần làm</a></li>
                <li role="presentation"><a href="#doing" aria-controls="profile" role="tab" data-toggle="tab">Đang triển khai</a></li>
                <li role="presentation"><a href="#done" aria-controls="exper" role="tab" data-toggle="tab">Hoàn thành</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="need">
                    <table class="table table-striped table-bordered  tbl-detail-work" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Dự án</th>
                                <th>Nội dung</th>
                                <th>Ngày bắt đầu</th>
                                <th>Ngày kết thúc</th>
                                <th>Trình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>01</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>02</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>03</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>04</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="doing">
                    <table class="table table-striped table-bordered nowrap tbl-detail-work" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Dự án</th>
                                <th>Nội dung</th>
                                <th>Ngày bắt đầu</th>
                                <th>Ngày kết thúc</th>
                                <th>Trình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>01</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>02</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>03</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>04</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="done">
                    <table class="table table-striped table-bordered nowrap tbl-detail-work" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Dự án</th>
                                <th>Nội dung</th>
                                <th>Ngày bắt đầu</th>
                                <th>Ngày kết thúc</th>
                                <th>Trình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>01</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">100%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>02</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">100%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>03</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">100%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>04</td>
                                <td>Be careful with this assumption</td>
                                <td>Be careful with this assumption careful with this assumption</td>
                                <td>02/03/2017</td>
                                <td>02/12/2017</td>
                                <td>
                                    <div class="progress-group">
                                        <span class="progress-number">100%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    //Table of employee manage form {menu personnel}
    $('#table-mobile').DataTable({
        responsive: true
    });
    if ($(window).width() < 1200) {
        $('.link-detail-work').click(function () {
            $('#mask , .works-detail').fadeIn(400);
        });
        $("button.close, #mask").click(function () {
            $('#mask , .works-detail').fadeOut(400);
        });
    }
    if ($(window).width() < 768) {
        $('.tbl-detail-work').DataTable({
            responsive: true
        });
    }

</script>
@endsection