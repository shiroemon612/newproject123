@extends('erp.layouts.app')

@section('content')
    <div class="group-breadcrumb">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <li id="menueml">Dự án</li>
            <li class="active">Danh sách nhân viên</li>
        </ol>
    </div>
    <div class="content-page-project">
        @include('erp.layouts.leftmenu')
        <div class="tab-content project-right">
            <div role="tabpanel" class="tab-pane active" id="employee">
                <div class="group-content">
                    <div class="alert alert-warning alert-dismissible">
                        <h4><i class=" fa fa-user"></i>Chọn nhân sự cho dự án</h4>
                        Những nhân viên sẽ tham gia vào dự án trên.
                        <div class="pull-right">
                            <p class="link-create-outsourced btn-green">
                                <i class="fa fa-plus"></i>
                                <a href="#" id="link-create" onclick="sp.addExEmpl()">Tạo nhân sự thuê ngoài</a>
                            </p>
                        </div>
                    </div>
                    <div class="group-create">
                        <form method="post" role="form" id="form-addNewEmployee">
                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 col-xs-12">
                                        <div class="form-group">
                                            <label>Họ tên:</label>
                                            <input name="name" id="name" type="text" class="form-control" placeholder="Họ tên..." />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-offset-3 col-xs-12">
                                        <div class="form-group">
                                            <label>Email:</label>
                                            <input name="email" id="email" type="text" class="form-control" placeholder="Email..." />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-offset-3 col-xs-12">
                                        <div class="form-group">
                                            <label>Số điện thoại:</label>
                                            <input name="phone" id="phone" type="text" class="form-control" placeholder="Số điện thoại..." />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3 col-xs-12" style="text-align: center;">
                                    <button type="button" class="btn-add-emp" >Thêm</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <form id="form-addEmployeePro" method="post" role="form"  action="{{ route('erp.project.updateStaffToProject') }}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <table id="tbl-create" class="table table-bordered text-center tbl-create" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Chức vụ</th>
                                <th>Ngày vào dự án</th>
                                <th>Ngày ra dự án</th>
                                <th>Ghi chú</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($projectStaff as $st)

                                {{--<form  method="post" role="form" action="{{ route('erp.project.updateStaffToProject') }}">--}}
                                <tr id="{{$st['id']}}" isout="{{ $st['staff']['is_out'] or 0 }}">

                                    <td>
                                        <div class="form-group">
                                            <input name="addName" type="text" class="form-control" value="{{$st['staff']['name']}}" disabled="disabled">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="addEmail" id="addEmail-{{$loop->index+1}}" type="text" class="form-control" placeholder="" value="{{ $st['email'] or "" }}" disabled="disabled"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="addPhone" id="addPhone-{{$loop->index+1}}" type="text" class="form-control" placeholder="" value="{{ $st['phone'] or "" }}" disabled="disabled"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input name="addJob" id="addJob-{{$loop->index+1}}" type="text" class="form-control" placeholder="Chức vụ" value="{{ $st['res'] or "" }}" disabled="disabled"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input data-provide="datepicker" class="form-control addStartDate" placeholder="" name="addStartDate" id="addStartDate-{{$loop->index+1}}" value="{{ $st['in'] or "" }}" disabled="disabled"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input data-provide="datepicker" class="form-control addEndDate" placeholder="" name="addEndDate" id="addEndDate-{{$loop->index+1}}" value="{{ $st['out'] or "" }}" disabled="disabled"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input class=" form-control textarea" placeholder="Nhập ghi chú..." name="addDes" id="addDes-{{$loop->index+1}}" disabled="disabled" value="{{ $st['note'] or "" }}">
                                        </div>
                                    </td>
                                    <td>
                                        <button type="button" class="edit-emp" onclick="sp.editEmpl(this)"><i class="fa fa-pencil"></i></button><button type="button" class="delete-emp" onclick="sp.deleteEmpl(this)"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                                {{--</form>--}}
                            @endforeach
                            <tr id="tr-addEmployeePro">
                                <td>
                                    <div class="form-group">
                                        <select name="addName" id="addName" class="chosen-select form-control addName" target-email="#addEmail" target-phone="#addPhone" placeholder="--chọn--">
                                            @foreach($staffs as $value)
                                                <option value="{{$value['_id']}}" opemail="{{$value['email']}}" opphone="{{$value['phone']}}" isout="{{ $value['isout'] or 0 }}" >{{$value['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>

                                <td>
                                    <div class="form-group">
                                        <input name="addEmail" id="addEmail" type="text" class="form-control" placeholder="an.ptm90@gmail.com" disabled="disabled"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="addPhone" id="addPhone" type="text" class="form-control" placeholder="0459631458" disabled="disabled"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input name="addJob" id="addJob" type="text" class="form-control" placeholder="Chức vụ" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input data-provide="datepicker" class="form-control addStartDate" placeholder="" name="addStartDate" id="addStartDate" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input data-provide="datepicker" class="form-control addEndDate" placeholder="" name="addEndDate" id="addEndDate" />
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input class=" form-control textarea" placeholder="Nhập ghi chú..." name="addDes" id="addDes" />
                                    </div>
                                </td>
                                <td>
                                    <button type="button" class="add-emp" onclick="sp.addEmpl()" ><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                            </tbody>

                        </table>

                    </form>
                    {{--<form id="form-addEmployeePro" method="post" role="form" action="{{ route('erp.project.addStaffToProject') }}">--}}
                    {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
                    {{--<table class="table table-bordered text-center tbl-create" cellspacing="0" width="100%">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                    {{--<th>Tên</th>--}}
                    {{--<th>Email</th>--}}
                    {{--<th>Phone</th>--}}
                    {{--<th>Chức vụ</th>--}}
                    {{--<th>Ngày vào dự án</th>--}}
                    {{--<th>Ngày ra dự án</th>--}}
                    {{--<th>Ghi chú</th>--}}
                    {{--<th></th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                    {{--<tr id="tr-addEmployeePro">--}}
                    {{--<td>--}}
                    {{--<div class="form-group">--}}
                    {{--<select name="addName" id="addName" class="chosen-select form-control addName" target-email="#addEmail" target-phone="#addPhone" placeholder="--chọn--">--}}
                    {{--@foreach($staffs as $value)--}}
                    {{--<option value="{{$value['_id']}}">{{$value['name']}}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--</td>--}}

                    {{--<td>--}}
                    {{--<div class="form-group">--}}
                    {{--<input name="addEmail" id="addEmail" type="text" class="form-control" placeholder="an.ptm90@gmail.com" />--}}
                    {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>--}}
                    {{--<div class="form-group">--}}
                    {{--<input name="addPhone" id="addPhone" type="text" class="form-control" placeholder="0459631458" />--}}
                    {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>--}}
                    {{--<div class="form-group">--}}
                    {{--<input name="addJob" id="addJob" type="text" class="form-control" placeholder="Chức vụ" />--}}
                    {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>--}}
                    {{--<div class="form-group">--}}
                    {{--<input data-provide="datepicker" class="form-control addStartDate" placeholder="" name="addStartDate" id="addStartDate" />--}}
                    {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>--}}
                    {{--<div class="form-group">--}}
                    {{--<input data-provide="datepicker" class="form-control addEndDate" placeholder="" name="addEndDate" id="addEndDate" />--}}
                    {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>--}}
                    {{--<div class="form-group">--}}
                    {{--<textarea class=" form-control textarea" placeholder="Nhập ghi chú..." name="addDes" id="addDes"></textarea>--}}
                    {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>--}}
                    {{--<button type="submit" class="add-emp" ><i class="fa fa-plus-circle"></i></button>--}}
                    {{--</td>--}}
                    {{--</tr>--}}
                    {{--</tbody>--}}
                    {{--</table>--}}
                    {{--</form>--}}
                </div>
            </div>
        {{--<script>--}}
        {{--$(".addName").change(function () {--}}
        {{--var id= $(this).val();--}}
        {{--var data = new FormData();--}}

        {{--var target_email = $(this).attr('target-email');--}}
        {{--var target_phone = $(this).attr('target-phone');--}}

        {{--data.append('_id', id);--}}
        {{--$.ajax({--}}
        {{--type : 'POST',--}}
        {{--dataType: "json",--}}
        {{--url  : '/erp/employer/getemployer',--}}
        {{--data : data,--}}
        {{--processData: false,--}}
        {{--contentType: false,--}}
        {{--success :  function(data)--}}
        {{--{--}}
        {{--var email = (data.email!==undefined) ? data.email : '';--}}
        {{--var phone = (data.phone!==undefined) ? data.phone : '';--}}
        {{--$(target_email).val(email);--}}
        {{--$(target_phone).val(phone);--}}
        {{--}--}}
        {{--});--}}
        {{--return false;--}}
        {{--})--}}
        {{--</script>--}}
        <!--<div role="tabpanel" class="tab-pane active" id="employee">
            <div class="group-content">
                <div class="alert alert-warning alert-dismissible">
                    <h4><i class=" fa fa-user"></i>Chọn nhân sự cho dự án</h4>
                    Những nhân viên sẽ tham gia vào dự án.
                    <div class="pull-right">
                        <p class="link-create-outsourced btn-green">
                            <i class="fa fa-plus"></i>
                            <a href="#" id="link-create">Tạo nhân sự thuê ngoài</a>
                        </p>
                    </div>
                </div>
                <div class="group-create">
                    <form method="post" role="form" id="form-addNewEmployee">
                        <fieldset>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Họ tên:</label>
                                        <input name="name" id="name" type="text" class="form-control" placeholder="Họ tên..." />
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Email:</label>
                                        <input name="email" id="email" type="text" class="form-control" placeholder="Email..." />
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3 col-xs-12">
                                    <div class="form-group">
                                        <label>Số điện thoại:</label>
                                        <input name="phone" id="phone" type="text" class="form-control" placeholder="Số điện thoại..." />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-3 col-xs-12" style="text-align: center;">
                                <button type="button" class="btn-add-emp" >Thêm</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <table class="table table-bordered text-center tbl-create" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Chức vụ</th>
                            <th>Ngày vào dự án</th>
                            <th>Ngày ra dự án</th>
                            <th>Ghi chú</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="tr-addEmployeePro">
                            <td>
                                <div class="form-group">
                                    <select name="addName" id="addName" class="chosen-select form-control addName" data-placeholder="--chọn--">
                                        <option value="Nguyen Van A">Nguyen Van A</option>
                                        <option value="Nguyen Van B">Nguyễn Văn B</option>
                                        <option value="Nguyen Van C">Nguyen Van C</option>
                                        <option value="Nguyen Van D">Nguyễn Văn D</option>
                                    </select>
                                </div>
                            </td>

                            <td>
                                <div class="form-group">
                                    <input name="addEmail" id="addEmail" type="text" class="form-control" placeholder="an.ptm90@gmail.com" />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input name="addPhone" id="addPhone" type="text" class="form-control" placeholder="0459631458" />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <select class="form-control addJob" name="addJob" id="addJob" data-placeholder="--chọn--">
                                        <option></option>
                                        <option>Chính thức</option>
                                        <option>Thử việc</option>
                                        <option>Bán thời gian</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input data-provide="datepicker" class="form-control addStartDate" placeholder="" name="addStartDate" id="addStartDate" />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input data-provide="datepicker" class="form-control addEndDate" placeholder="" name="addEndDate" id="addEndDate" />
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <textarea class=" form-control textarea" placeholder="Nhập ghi chú..." name="addDes" id="addDes"></textarea>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="add-emp" ><i class="fa fa-plus-circle"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> -->
            <!--////////////////////////////////////////////////////////// -->

            <script src="{{ asset('js/staff.js') }}"></script>
            <script src="{{ asset('js/staff.employee.js') }}"></script>
            <script>
                var sp = Staff_project();
                $(document).ready(function () {


                    $('#tr-addEmployeePro #addName').change(function () {
                        var addemail = $('#tr-addEmployeePro #addName option:selected').attr("opemail");
                        var addphone = $('#tr-addEmployeePro #addName option:selected').attr("opphone");
                        $('#tr-addEmployeePro #addEmail').val(addemail);
                        $('#tr-addEmployeePro #addPhone').val(addphone);
                    });
                    $('#tr-addEmployeePro #addName').trigger('change');
                    $(".add-emp").click(function (e) {
                        e.preventDefault();
                        sp.addEmpl('tr-addEmployeePro','tbl-create');
                    });
                    $(".btn-add-emp").click(function (e) {
                        e.preventDefault();
                        sp.addExEmpl();
                    });
                    $(function () {
                        var token = $("meta[name='csrf-token']").attr("content");

                        $("#form-addNewEmployee").validate({
                            rules: {
                                name: "required",
                                email: {
                                    required: true,
                                    email: true,
                                    async: false,
                                    remote: {
                                        type: 'POST',
                                        url: '/erp/employer/check',
                                        data: {
                                            '_token' : token
                                        }
                                    }
                                },
                                phone: {
                                    required: true,
                                    number: true,
                                    minlength: 10
                                }
                            },
                            messages: {
                                name: "Vui lÃ²ng nháº­p há» tÃªn",
                                email: {
                                    required: "Vui lÃ²ng nháº­p Email",
                                    email: "Ãá»‹a chá»‰ email khÃ´ng há»£p lá»‡",
                                    remote: "Email Ä‘Ã£ tá»“n táº¡i",
                                },
                                phone: {
                                    required: "Vui lÃ²ng nháº­p Sá»‘ Ä‘iá»‡n thoáº¡i",
                                    number: "Sá»‘ Ä‘iá»‡n thoáº¡i pháº£i lÃ  sá»‘",
                                }
                            }

                        });
                    });
                    //popup create new employee in {menu project}
                    $('#link-create').click(function () {
                        $(".group-create").toggleClass("open");

                    });

                });

            </script>
@endsection
