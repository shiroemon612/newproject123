@extends('erp.layouts.app')

@section('content')
    <?php
    if (Session::has('idProject')) {
        $idProject = Session::get('idProject');
    }
    ?>
    <div class="group-breadcrumb">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <li id="menueml">Dự án</li>
            <li class="active">Danh sách công việc</li>
        </ol>
    </div>
    <div class="content-page-project">
        @include('erp.layouts.leftmenu')
        <div class="tab-content project-right ">
            <div role="tabpanel" class="tab-pane active" id="work">
                <div class="header-work">
                    <div class="group-title">
                        <h3 class="title-page">Danh sách công việc</h3>
                        <h5 class="des-page"></h5>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <p class="btn-box btn-green upload-file">
                                <i class="fa fa-upload"></i>
                                <input class="btn-icon choose-file" type="file" value="Upload"/>
                            </p>
                            <p class="btn-box btn-green">
                                <i class="fa fa-download"></i>
                                <input class="btn-icon" type="button" value="Download"
                                       onclick="location.href = '{{ url('erp/project/worklist/download/'.$idProject) }}';"/>
                            </p>
                        </div>
                        <ul class="nav nav-tabs tab-work">
                            <li><a href="{{ url('erp/project/work/'. $idProject) }}" id="link-view1"><i
                                            class="fa fa-th-large" aria-hidden="true"></i></a></li>
                            <li><a href="{{ url('erp/project/worklist/'. $idProject) }}" id="link-view2"><i
                                            class="fa fa-th-list" aria-hidden="true"></i></a></li>
                            <li><a href="{{ url('erp/project/workstaff/'. $idProject) }}" id="link-view3"><i
                                            class="fa fa-user"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content">
                    <div id="view2" class="tab-pane fade in active">
                        <div class="rTable">
                            <div class="" id="table-master">
                                <div class="rTableHead stt">STT</div>
                                <div class="rTableHead name">Tên công việc</div>
                                <div class="rTableHead unit">ĐVT</div>
                                <div class="rTableHead weight">Khối lượng</div>
                                <div class="rTableHead price">Đơn giá</div>
                                <div class="rTableHead money">Thành tiền</div>
                                <div class="rTableHead time"><a href="#"><i class="fa fa-angle-left"></i></a>
                                    <div class="box-time">Thời gian</div>
                                    <div class="start-time">Bắt đầu</div>
                                    <div class="end-time">Kết thúc</div>
                                </div>
                                <div class="rTableHead manager">Phụ trách</div>
                                <div class="rTableHead process">Tiến trình</div>
                                <div class="rTableHead action">Xử lý</div>
                            </div>
                            <!-- foreach -->
                            <?php use App\Http\Controllers\ERP\Project\ProjectWorkListController;
                            echo ProjectWorkListController::render(); ?>
                            {{--<div class=" parents">--}}
                                {{--<div class="rTableRow">--}}
                                    {{--<div class="rTableCell"><span class="number"><a href="#"></a>1</span></div>--}}
                                    {{--<div class="rTableCell">Tên công việc</div>--}}
                                    {{--<div class="rTableCell">ĐVT</div>--}}
                                    {{--<div class="rTableCell">Khối lượng</div>--}}
                                    {{--<div class="rTableCell price">Đơn giá</div>--}}
                                    {{--<div class="rTableCell money">Thành tiền</div>--}}
                                    {{--<div class="rTableCell">--}}
                                        {{--<div class="start-time">20/05/2017</div>--}}
                                        {{--<div class="end-time">20/05/2017</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="rTableCell">--}}
                                        {{--<a href="#" class="img-member" style="background-image: url({{ asset("img_empl/default.jpg") }})"></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="rTableCell">--}}
                                        {{--<div class="progress-group">--}}
                                            {{--<span class="progress-text">--}}
                                                {{--<span class="start">02/24/2017</span> <span--}}
                                                        {{--class="end">03/24/2017</span>--}}
                                            {{--</span>--}}
                                            {{--<span class="progress-number">80%</span>--}}

                                            {{--<div class="progress sm">--}}
                                                {{--<div class="progress-bar progress-bar-aqua" style="width: 80%"></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="rTableCell">--}}
                                        {{--<p class="btn-box btn-trans">--}}
                                            {{--<i class="fa fa-plus"></i>--}}
                                            {{--<a href="#" data-toggle="modal" data-target="#createJobChild"></a>--}}
                                        {{--</p>--}}
                                        {{--<p class="btn-box btn-trans">--}}
                                            {{--<i class="fa fa-pencil"></i>--}}
                                            {{--<a href="#" data-toggle="modal" data-target="#editJobChild"></a>--}}
                                        {{--</p>--}}
                                        {{--<button type="button" class="delete-emp btn-trans"><i class="fa fa-trash-o"></i>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- child --->--}}
                                {{--<div class="childs">--}}
                                    {{--<!-- foreach-->--}}
                                    {{--<div class="rTableRow">--}}
                                        {{--<div class="rTableCell">--}}
                                            {{--<span class="number"><a href="#"></a>1.1</span>--}}
                                        {{--</div>--}}
                                        {{--<div class="rTableCell">Tên công việc</div>--}}
                                        {{--<div class="rTableCell">ĐVT</div>--}}
                                        {{--<div class="rTableCell">Khối lượng</div>--}}
                                        {{--<div class="rTableCell price">Đơn giá</div>--}}
                                        {{--<div class="rTableCell money">Thành tiền</div>--}}
                                        {{--<div class="rTableCell">--}}
                                            {{--<div class="start-time">20/05/2017</div>--}}
                                            {{--<div class="end-time">20/05/2017</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="rTableCell">--}}
                                            {{--<a href="#" class="img-member" style="background-image: url({{ asset("img_empl/default.jpg") }})"></a>--}}
                                        {{--</div>--}}
                                        {{--<div class="rTableCell">--}}
                                            {{--<div class="progress-group">--}}
                                            {{--<span class="progress-text">--}}
                                                {{--<span class="start">02/24/2017</span> <span--}}
                                                        {{--class="end">03/24/2017</span>--}}
                                            {{--</span>--}}
                                                {{--<span class="progress-number">80%</span>--}}

                                                {{--<div class="progress sm">--}}
                                                    {{--<div class="progress-bar progress-bar-aqua"--}}
                                                         {{--style="width: 80%"></div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="rTableCell">--}}
                                            {{--<p class="btn-box btn-trans">--}}
                                                {{--<i class="fa fa-plus"></i>--}}
                                                {{--<a href="#" data-toggle="modal" data-target="#createJobChild"></a>--}}
                                            {{--</p>--}}
                                            {{--<p class="btn-box btn-trans">--}}
                                                {{--<i class="fa fa-pencil"></i>--}}
                                                {{--<a href="#" data-toggle="modal" data-target="#editJobChild"></a>--}}
                                            {{--</p>--}}
                                            {{--<button type="button" class="delete-emp btn-trans"><i--}}
                                                        {{--class="fa fa-trash-o"></i></button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!-- end foreach-->--}}
                                {{--</div>--}}
                                {{--<!-- end child -->--}}
                            {{--</div>--}}
                            {{--<!-- endforeach -->--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- create form -->
    <div class="modal fade" id="createJobChild" tabindex="-1" role="dialog" aria-labelledby="myCreateJobChild">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-createJobChild" action="{{ route('erp.project.workListAddChild') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateJobChild">Thêm công việc con</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên công việc lớn:</label>
                                <input type="hidden" name="parentId" id="parentId"/>
                                <input type="text" class="form-control" name="parentTask" id="parentTask"
                                       placeholder="" disabled="disabled" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên công việc con:</label>
                                <input type="text" class="form-control" name="nameJobChild" id="nameJobChild"
                                       placeholder="Nhập tên công việc..."/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian bắt đầu:</label>
                                <input data-provide="datepicker" class="form-control " name="startDateJobChild"
                                       id="startDateJobChild" placeholder="dd/mm/yyyy"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian kết thúc:</label>
                                <input data-provide="datepicker" class="form-control " name="endDateJobChild"
                                       id="endDateJobChild" placeholder="dd/mm/yyyy"/>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">Thành viên:</label>
                                <select name="member" data-placeholder="Choose" class="chosen-select form-control">
                                    <option value=""></option>
                                    <option value="123456789">United States</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control" name="description" rows="3" placeholder="Nhập mô tả..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 50px;">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <!-- edit form -->
    <div class="modal fade" id="editJobChild" tabindex="-1" role="dialog" aria-labelledby="myEditJobChild">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-editJobChild">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myEditJobChild">Sửa công việc con</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên công việc lớn:</label>
                                <input type="text" class="form-control" name="" id="" placeholder="" disabled/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên công việc con:</label>
                                <input type="text" class="form-control" name="nameEditTask" id="nameEditTask"
                                       placeholder="Nhập tên công việc..."/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian bắt đầu:</label>
                                <input data-provide="datepicker" class="form-control " name="startDateTask"
                                       id="startDateTask" placeholder="dd/mm/yyyy"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian kết thúc:</label>
                                <input data-provide="datepicker" class="form-control " name="endDateTask"
                                       id="endDateTask" placeholder="dd/mm/yyyy"/>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Thành viên:</label>
                                <select data-placeholder="Choose" class="chosen-select form-control">
                                    <option value=""></option>
                                    <option value="United States">United States</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control" rows="3" placeholder="Nhập mô tả..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <!--////////////////////////////////////////////////////////// -->
    <script src="{{ asset('js/staff.js') }}"></script>
    <script src="{{ asset('js/worklist.js') }}"></script>
    <script>
        $(document).ready(function () {

            $(".upload-file .choose-file").change(function () {
                var fileExtension = ['xls', 'xlsx'];
                var empGroup = $(".group-employee");
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Only formats are allowed : " + fileExtension.join(', '));
                    return;
                }
                var data = new FormData();
                data.append('file', $('.upload-file .choose-file').prop('files')[0]);
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/erp/project/worklist/upload',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        alert('uploaded !');
                    }
                });
                return false;
            });
        });

        function addEmployer(id, name) {
            document.getElementById("parentId").value = id;
            document.getElementById("parentTask").value = name;
        }
    </script>
@endsection