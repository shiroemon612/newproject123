﻿@extends('erp.layouts.app')

@section('content')
    <div class="group-breadcrumb">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <li id="menueml">Dự án</li>
            <li class="active">Danh sách dự án</li>
        </ol>
        <ol class="breadcrumb pull-right">
            <li>
                <form action="{{ URL('erp/project/search') }}" method="get" name="searchProject" id="searchProject">
                    <p class="input-search">
                        <i class="fa fa-search"></i>
                        <input type="text" name="nameProject" class="form-control" placeholder="Nhập thông tin tìm kiếm..." />
                        <input class="btn-icon" type="submit" value="" />
                    </p>
                </form>
            </li>
            <li><a href="#" data-toggle="modal" data-target="#createProject"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-th-list" aria-hidden="true"></i></a></li>
            <li class="active"><i class="fa fa-user"></i></li>
        </ol>
    </div>
    <div class="content-page">
    <!--list project-->
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $err)
                    {{ $err }}<br>
                @endforeach
            </div>
        @endif
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <div class="group-pro">
            @if(isset($project))
                <?php $i = 0; ?>
                @foreach($project as $item)
                <div class="pro-item">
                    <div class="pro-content">
                        <a href="{{ URL::route('erp.project.getRenderInfoProject', ['id' => $item['_id']]) }}" class="name">{{ $item['name'] or '' }}
                        </a>
                        <p>- Thời hạn: <b>{{ $item['end'] or '' }}</b></p>
                        <p>- Trưởng dự án: <b>{{ $leader[$i]['leader'] or " " }}</b></p>
                        <div class="group-menber">
                            <?php $count = 0; ?>
                            @if(isset($item['project_staff']))
                            @foreach($item['project_staff'] as $staff)
                                @if($count == 4)
                                    @break;
                                @endif
                                <?php
			                        $imgurl = 'img_empl/' . Session::get('dbname') . '/' . $staff['id'] . '.jpg';
	                                $count++;
		                        ?>
                                <a href="#"><img class="img-member" src="{{ asset($imgurl) }}" onerror="javascript:imgError(this)" /></a>
                            @endforeach
                            @endif
                        </div>
                        <p>- Tiến trình dự án:</p>
                        <div class="progress-group">
                            <span class="progress-text">
                                <span class="start">{{ $item['start'] or '' }}</span> <span class="end">{{ $item['end'] or '' }}</span>
                            </span>
                            <span class="progress-number">80%</span>

                            <div class="progress sm">
                                <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button id="Label" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="#" class="onClick" data-id="{{ $item['_id'] }}" data-toggle="modal" data-target="#editProject" url="{{ URL::route('ajax.erp.project.editProject') }}"><i class="fa fa-pencil "></i><span>Sửa</span></a></li>
                            <li><a href="{{ URL('erp/project/delete/'.$item['_id']) }}"><i class="fa fa-trash-o "></i><span>Xóa</span></a></li>
                        </ul>
                    </div>
                </div>
		                <?php $i++; ?>
                @endforeach
            @endif
        </div>
    </div>

    <!--create project-->
    <div class="modal fade" id="createProject" tabindex="-1" role="dialog" aria-labelledby="myCreateProject">
        <div class="modal-dialog" role="document">
            <form name="createProject" method="post" action="{{ URL('erp/project/create') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myCreateProject">Tạo dự án</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Tên dự án:</label>
                                    <input type="text" class="form-control" name="name" id="nameproject"
                                           placeholder="Tên Dự Án"/>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Trưởng dự án:</label>
                                    <select name="staff_id" data-placeholder="Trưởng dự án" class="chosen-select form-control">
                                        @if(isset($staffs))
                                            @foreach($staffs as $value)
                                                <option value="{{ $value['_id'] }}">{{ $value['name'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Thời gian bắt đầu:</label>
                                    <input name="start" id="start" data-provide="datepicker" class="form-control" placeholder="mm/dd/yyyy"/>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Thời gian kết thúc:</label>
                                    <input name="end" id="end" data-provide="datepicker" class="form-control" placeholder="mm/dd/yyyy"/>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div>
                                    <label>Mô tả:</label>
                                    <textarea name="description" class="form-control" rows="3" placeholder="Nhập mô tả..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                        <button type="submit" class="btn btn-success">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="editProject" tabindex="-1" role="dialog" aria-labelledby="myEditProject">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" name="form-editProject" role="form" id="form-editProject" action="{{ URL::route('ajax.erp.project.editProject') }}">
                <div class="idProject">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myEditProject">
                        Sửa dự án
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên dự án:<span class="note">(*)</span></label>
                                <div class="name-project">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Trưởng dự án:</label>
                                <select data-placeholder="Choose" class="chosen-select form-control" name="manage" id="manage">
                                    @if(isset($staffs))
                                        @foreach($staffs as $value)
                                            <option id="{{ $value['_id'] }}" value="{{ $value['_id'] }}">{{ $value['name'] }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian bắt đầu:</label>
                                <div class="startDate">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian kết thúc:</label>
                                <div class="endDate">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label>Mô tả:</label>
                                <div class="descriptionProject">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function () {
            $('.chosen-select').chosen();
            $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
        });
    </script>
    <script>
        $(function() {
            jQuery.validator.addMethod("greaterThan",
                function (value, element, params) {
                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) >= new Date($(params).val());
                    }
                    return isNaN(value) && isNaN($(params).val())
                        || (Number(value) >= Number($(params).val()));
                }, 'Must be greater than {0}.');

            $("form[name='createProject']").validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 3
                    },
                    start: {
                        required:true
                    },
                    end: {
                        required:true,
                        greaterThan: "#start"
                    }

                },
                messages: {
                    name: {
                        required: "Please enter your name project",
                        minlength: "Your name project must be at least 3 characters long"
                    },
                    start: {
                        required: "Please enter your start date"
                    },
                    end: {
                        required: "Please enter your end date",
                        greaterThan: "End date is greater than start date"
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
    <script>
        //validate create new employee {menu project}
        $(function () {
            $("#form-editProject").validate({
                rules: {
                    nameEdit: {
                        required: true,
                        minlength: 3
                    },
                    startEdit: {
                        required:true
                    },
                    endEdit: {
                        required:true,
                        greaterThan: "#startEdit"
                    }

                },
                messages: {
                    nameEdit: {
                        required: "Please enter your name project",
                        minlength: "Your name project must be at least 3 characters long"
                    },
                    startEdit: {
                        required: "Please enter your start date"
                    },
                    endEdit: {
                        required: "Please enter your end date",
                        greaterThan: "End date is greater than start date"
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
    <script>
        function imgError(img) {
            img.error = "";
            img.src =window.location.origin + '/img_empl/default.jpg';
        }
    </script>
@endsection 