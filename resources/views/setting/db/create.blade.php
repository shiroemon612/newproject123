@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('setting.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Create New Database</div>
                    <div class="panel-body">
                        <a href="{{ url('/setting/db') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form method="post" action="/setting/db" accept-charset="UTF-8" class="form-horizontal">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <input name="dbname" type="text">
                        <button type="submit" value="Submit">Submit</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
