@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Quản lý cơ sở dữ liệu</div>
                    <div class="panel-body">
                        <a href="{{ url('/setting/db/create') }}" class="btn btn-success btn-sm" title="Add New Post">
                            <i class="fa fa-plus" aria-hidden="true"></i> Thêm CSDL
                        </a>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>Tên</th><th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($posts[0] as $item)
                                    <tr>
                                        <td> {{ $item }} </td>
                                        <td>
                                            <a href="{!! url('/setting/db/'.$item.'/edit') !!}" title="Edit">
                                            <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Chọn </button>
                                            </a>
                                            <a href="{!! url('/setting/db/delete') !!}" title="Delete"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Xóa</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
